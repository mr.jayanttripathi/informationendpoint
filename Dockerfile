# Start with a base image containing Java runtime
FROM openjdk:8

# Add Maintainer Info
LABEL maintainer="Mr.JayantTripathi@gmail.com"

# Add a volume pointing to /tmp
VOLUME /tmp

# The application's jar file
ARG JAR_FILE=target/informationendpoint.jar

# Add the application's jar to the container
ADD ${JAR_FILE} informationendpoint.jar

# Run the jar file 
ENTRYPOINT ["java","-jar","/informationendpoint.jar"]
