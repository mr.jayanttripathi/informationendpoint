package com.personal.informationendpoint.dao.mapper;

import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class experienceInfoMapperTest {



    @InjectMocks
    experienceInfoMapper experienceInfoMapperObj;

    @Mock
    ResultSet resultSet;

    @Before
    public void init() throws IOException {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void mapRowTest() throws SQLException {

        ExperienceInfo  experienceInfo=new ExperienceInfo();

        Mockito.when(resultSet.getString("Project")).thenReturn("Home Delivery");
        Mockito.when(resultSet.getString("ClientCompany")).thenReturn("2019");
        Mockito.when(resultSet.getString("ContractingCompany")).thenReturn("College");
        Mockito.when(resultSet.getString("EndDate")).thenReturn("1");
        Mockito.when(resultSet.getString("PersonID")).thenReturn("Pass");
        Mockito.when(resultSet.getString("Project")).thenReturn("Pass");
        Mockito.when(resultSet.getString("Responsibilities")).thenReturn("Pass");
        Mockito.when(resultSet.getString("Roles")).thenReturn("Pass");
        Mockito.when(resultSet.getString("StartDate")).thenReturn("Pass");
        Mockito.when(resultSet.getString("Team")).thenReturn("Pass");
        Mockito.when(resultSet.getString("TechStack")).thenReturn("Pass");



        experienceInfo = experienceInfoMapperObj.mapRow(resultSet,1);

    Assert.assertEquals(experienceInfo.getProject(), "Pass");
    }
}
