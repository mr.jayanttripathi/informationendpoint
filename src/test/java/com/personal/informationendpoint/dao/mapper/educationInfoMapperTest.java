package com.personal.informationendpoint.dao.mapper;

import com.personal.informationendpoint.pojo.response.EducationInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class educationInfoMapperTest {

    @InjectMocks
    educationInfoMapper educationInfoMapperObj;

    @Mock
    ResultSet resultSet;

    @Before
    public void init() throws IOException {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void mapRowTest() throws SQLException {

        EducationInfo  educationInfo=new EducationInfo();

        Mockito.when(resultSet.getString("Degree")).thenReturn("Master");
        Mockito.when(resultSet.getString("GraduationDate")).thenReturn("2019");
        Mockito.when(resultSet.getString("Institution")).thenReturn("College");
        Mockito.when(resultSet.getString("PersonID")).thenReturn("1");
        Mockito.when(resultSet.getString("Result")).thenReturn("Pass");

        educationInfo = educationInfoMapperObj.mapRow(resultSet,1);

        Assert.assertEquals(educationInfo.getDegree(),"Master");
    }
}
