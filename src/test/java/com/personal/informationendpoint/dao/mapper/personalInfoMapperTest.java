package com.personal.informationendpoint.dao.mapper;

import com.personal.informationendpoint.pojo.response.PersonalInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class personalInfoMapperTest {

    @InjectMocks
    personalInfoMapper personalInfoMapperObj;

    @Mock
    ResultSet resultSet;

    @Before
    public void init() throws IOException {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void mapRowTest() throws SQLException {

        PersonalInfo personalInfo=new PersonalInfo();
        Mockito.when(resultSet.getString("FirstName")).thenReturn("Jayant");
        Mockito.when(resultSet.getString("MiddleName")).thenReturn("Jayant");
        Mockito.when(resultSet.getString("LastName")).thenReturn("Jayant");
        Mockito.when(resultSet.getString("Suite")).thenReturn("Jayant");
        Mockito.when(resultSet.getString("Street")).thenReturn("Jayant");
        Mockito.when(resultSet.getString("City")).thenReturn("Jayant");
        Mockito.when(resultSet.getString("State")).thenReturn("Jayant");
        Mockito.when(resultSet.getString("Country")).thenReturn("Jayant");
        Mockito.when(resultSet.getString("PhoneNUmber")).thenReturn("Jayant");
       // Mockito.when(resultSet.getDate("DateofBirth")).thenReturn();
        Mockito.when(resultSet.getString("EmailAddress")).thenReturn("Jayant");



        personalInfo = personalInfoMapperObj.mapRow(resultSet,1);

        Assert.assertEquals(personalInfo.getFirstName(), "Jayant");
    }
}
