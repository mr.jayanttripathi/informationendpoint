package com.personal.informationendpoint.dao.impl;

import com.personal.informationendpoint.dao.mapper.educationInfoMapper;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class educationInfoDaoImplTest {

    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplateObj;

    @InjectMocks
    educationInfoDaoImpl educationInfoDaoObj;

    List<EducationInfo> educationInfoObj;

    InputMaster inputMaster;

    @Before
    public void init() throws IOException {
        MockitoAnnotations.initMocks(this);
        //query= IOUtils.toString(this.getClass().getResourceAsStream(query));

        EducationInfo educationInfo=new EducationInfo();
        educationInfo.setResult("");
        educationInfo.setPersonID(1);
        educationInfo.setInstitution("College");
        educationInfo.setGraduationDate("");
        educationInfo.setDegree("");

        educationInfoObj=new ArrayList<>();
        educationInfoObj.add(educationInfo);
    }

    @Test
    public void initMethodTest() throws IOException {
        ReflectionTestUtils.setField(educationInfoDaoObj, "query", "/DatabaseQueries/educationInfoSql.sql");
        educationInfoDaoObj.init();
        String result=ReflectionTestUtils.getField(educationInfoDaoObj, "query").toString();
        Assert.assertNotEquals(result,"/DatabaseQueries/educationInfoSql.sql");

    }

    @Test
    public void fetchRequestedDataTest()
    {
        Map<String, Integer> parameterSource= new HashMap<>();
        parameterSource.put("personalID",1);
        EducationInfo educationInfo=new EducationInfo();
        List<EducationInfo> data=new ArrayList<>();
        Mockito.when(educationInfoDaoObj.namedParameterJdbcTemplateObj.query("",parameterSource,new educationInfoMapper())).thenReturn(educationInfoObj);

        data= educationInfoDaoObj.fetchRequestedData(inputMaster);
        Assert.assertEquals(0,data.size());

    }
}
