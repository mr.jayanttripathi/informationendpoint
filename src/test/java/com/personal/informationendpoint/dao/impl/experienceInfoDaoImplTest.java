package com.personal.informationendpoint.dao.impl;

import com.personal.informationendpoint.dao.mapper.experienceInfoMapper;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.*;

public class experienceInfoDaoImplTest {



    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplateObj;

    @InjectMocks
    experienceInfoDaoImpl experienceInfoDaoObj;

    List<ExperienceInfo> experienceInfoObj;

    InputMaster inputMaster;

    @Before
    public void init() throws IOException {
        MockitoAnnotations.initMocks(this);
        //query= IOUtils.toString(this.getClass().getResourceAsStream(query));

        ExperienceInfo experienceInfo=new ExperienceInfo();
        experienceInfo.setTechStack("");
        experienceInfo.setTeam("Home Delivery");
        experienceInfo.setStartDate(new Date());
        experienceInfo.setRoles("");
        experienceInfo.setResponsibilities("");
        experienceInfo.setProject("");
        experienceInfo.setPersonID(1);
        experienceInfo.setEndDate(new Date());
        experienceInfo.setContractingCompany("");
        experienceInfo.setClientCompany("");

        experienceInfoObj= new ArrayList<>();
        experienceInfoObj.add(experienceInfo);
    }

    @Test
    public void initMethodTest() throws IOException {
        ReflectionTestUtils.setField(experienceInfoDaoObj, "query", "/DatabaseQueries/educationInfoSql.sql");
        experienceInfoDaoObj.init();
        String result=ReflectionTestUtils.getField(experienceInfoDaoObj, "query").toString();
        Assert.assertNotEquals(result,"/DatabaseQueries/educationInfoSql.sql");

    }

    @Test
    public void fetchRequestedDataTest()
    {
        Map<String, Integer> parameterSource= new HashMap<>();
        parameterSource.put("personalID",1);
        ExperienceInfo educationInfo=new ExperienceInfo();
        List<ExperienceInfo> data=new ArrayList<>();
        Mockito.when(experienceInfoDaoObj.namedParameterJdbcTemplateObj.query("",parameterSource,new experienceInfoMapper())).thenReturn(experienceInfoObj);

        data= experienceInfoDaoObj.fetchRequestedData(inputMaster);
        Assert.assertEquals(0,data.size());

    }
}
