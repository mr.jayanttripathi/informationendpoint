package com.personal.informationendpoint.dao.impl;

import com.personal.informationendpoint.dao.mapper.personalInfoMapper;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.PersonalInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.*;

public class personalInfoDaoImplTest {




    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplateObj;

    @InjectMocks
    personalInfoDaoImpl personalInfoDaoObj;

    PersonalInfo personalInfo;

    InputMaster inputMaster;

    @Before
    public void init() throws IOException {
        MockitoAnnotations.initMocks(this);
        //query= IOUtils.toString(this.getClass().getResourceAsStream(query));

        personalInfo=new PersonalInfo();
        personalInfo.setFirstName("Jayant");
        personalInfo.setCity("Phoenix");
        personalInfo.setCountry("United States");
        personalInfo.setDateofBirth(new Date());
        personalInfo.setEmailAddress("Mr.JayantTripathi@Gmail.com");
        personalInfo.setLastName("Tripathi");
        personalInfo.setMiddleName("");
        personalInfo.setPhoneNUmber("6825600224");
        personalInfo.setState("");
        personalInfo.setStreet("43rd");
        personalInfo.setSuite("215");
    }

    @Test
    public void initMethodTest() throws IOException {
        ReflectionTestUtils.setField(personalInfoDaoObj, "query", "/DatabaseQueries/educationInfoSql.sql");
        personalInfoDaoObj.init();
        String result=ReflectionTestUtils.getField(personalInfoDaoObj, "query").toString();
        Assert.assertNotEquals(result,"/DatabaseQueries/educationInfoSql.sql");

    }

    @Test
    public void fetchRequestedDataTest()
    {
        Map<String, Integer> parameterSource= new HashMap<>();
        parameterSource.put("personalID",1);
        PersonalInfo data=new PersonalInfo();
        Mockito.when(personalInfoDaoObj.namedParameterJdbcTemplateObj.queryForObject("",parameterSource,new personalInfoMapper())).thenReturn(personalInfo);

        data= personalInfoDaoObj.fetchRequestedData(inputMaster);
        Assert.assertNull(data);

    }
}
