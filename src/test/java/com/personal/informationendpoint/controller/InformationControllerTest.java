package com.personal.informationendpoint.controller;


import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.handler.educationInfohandler;
import com.personal.informationendpoint.handler.persoanlInfohandler;
import com.personal.informationendpoint.handler.ExperienceInfohandler;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;
import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import com.personal.informationendpoint.pojo.response.PersonalInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

public class InformationControllerTest {

    @InjectMocks
    private InformationController informationController;

    @Mock
    private persoanlInfohandler persoanlInfohandlerObj;

    @Mock
    private educationInfohandler educationInfohandlerObj;

    @Mock
    private ExperienceInfohandler experienceInfohandlerObj;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getPersonalInfoPassTest() throws Exception {


       InputMaster inputMaster= new InputMaster();
       inputMaster.setPersonID("1");
       PersonalInfo personalInfo = informationController.getPersonalInfo(inputMaster);
       Assert.assertNull(personalInfo);
    }

    @Test(expected = customUncheckedException.class)
    public void getPersonalInfoFailTest() throws Exception {


        InputMaster inputMaster= new InputMaster();
        inputMaster.setPersonID("1");
        Mockito.doThrow(customUncheckedException.class).when(persoanlInfohandlerObj).handle(inputMaster);
        PersonalInfo personalInfo = informationController.getPersonalInfo(inputMaster);

    }

    @Test
    public void getEducationInfoPassTest() {

        InputMaster inputMaster= new InputMaster();
        inputMaster.setPersonID("1");
        List<EducationInfo> educationInfo = informationController.getEducationInfo(inputMaster);
        Assert.assertNotNull(educationInfo);
    }

    @Test(expected = customUncheckedException.class)
    public void getEducationInfoFailTest() {

        InputMaster inputMaster= new InputMaster();
        inputMaster.setPersonID("1");
        Mockito.doThrow(customUncheckedException.class).when(educationInfohandlerObj).handle(inputMaster);
        List<EducationInfo> educationInfo = informationController.getEducationInfo(inputMaster);

    }

    @Test
    public void getExperienceInfoPassTest() {
        InputMaster inputMaster= new InputMaster();
        inputMaster.setPersonID("1");
        List<ExperienceInfo> experienceInfo = informationController.getExperienceInfo(inputMaster);
        Assert.assertNotNull(experienceInfo);
    }

    @Test(expected = customUncheckedException.class)
    public void getExperienceInfoFailTest() {
        InputMaster inputMaster= new InputMaster();
        inputMaster.setPersonID("1");
        Mockito.doThrow(customUncheckedException.class).when(experienceInfohandlerObj).handle(inputMaster);
        List<ExperienceInfo> experienceInfo = informationController.getExperienceInfo(inputMaster);

    }

}
