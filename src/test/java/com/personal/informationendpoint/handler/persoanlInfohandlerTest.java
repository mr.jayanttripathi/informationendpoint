package com.personal.informationendpoint.handler;

import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import com.personal.informationendpoint.pojo.response.PersonalInfo;
import com.personal.informationendpoint.service.experienceInfoService;
import com.personal.informationendpoint.service.personalInfoService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class persoanlInfohandlerTest {

    @InjectMocks
    private  persoanlInfohandler persoanlInfohandlerObj;

    @Mock
    private personalInfoService personalInfoServiceObj;


    PersonalInfo personalInfo;

    @Before
    public void init()
    {
        MockitoAnnotations.initMocks(this);
        personalInfo=new PersonalInfo();
        personalInfo.setFirstName("Jayant");
        personalInfo.setCity("Phoenix");
        personalInfo.setCountry("United States");
        personalInfo.setDateofBirth(new Date());
        personalInfo.setEmailAddress("Mr.JayantTripathi@Gmail.com");
        personalInfo.setLastName("Tripathi");
        personalInfo.setMiddleName("");
        personalInfo.setPhoneNUmber("6825600224");
        personalInfo.setState("");
        personalInfo.setStreet("43rd");
        personalInfo.setSuite("215");
    }

    @Test
    public void handlePassTest(){

        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");

        Mockito.when(personalInfoServiceObj.fetchRequiredData(inputMasterObj)).thenReturn(personalInfo);
        PersonalInfo personalInfoExpected=persoanlInfohandlerObj.handle(inputMasterObj);
        Assert.assertEquals(personalInfo.getFirstName(),personalInfoExpected.getFirstName());
    }

    @Test(expected = customUncheckedException.class)
    public void handleFailTest(){

        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");
        Mockito.doThrow(customUncheckedException.class).when(personalInfoServiceObj).fetchRequiredData(inputMasterObj);
        PersonalInfo personalInfo=persoanlInfohandlerObj.handle(inputMasterObj);

    }

}
