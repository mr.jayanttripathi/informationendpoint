package com.personal.informationendpoint.handler;

import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;
import com.personal.informationendpoint.service.educationInfoService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class educationInfohandlerTest {

    @InjectMocks
    private educationInfohandler educationInfohandlerObj;

    @Mock
    private educationInfoService educationInfoServiceObj;

    List<EducationInfo> educationInfoObj;

    @Before
    public void init()
    {
            MockitoAnnotations.initMocks(this);

            EducationInfo educationInfo=new EducationInfo();
            educationInfo.setResult("");
            educationInfo.setPersonID(1);
            educationInfo.setInstitution("College");
            educationInfo.setGraduationDate("");
            educationInfo.setDegree("");

        educationInfoObj=new ArrayList<>();
        educationInfoObj.add(educationInfo);

    }



    @Test
    public void handlePassTest(){

        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");

        Mockito.when(educationInfoServiceObj.fetchRequiredData(inputMasterObj)).thenReturn(educationInfoObj);

        List<EducationInfo> educationInfo=educationInfohandlerObj.handle(inputMasterObj);
        Assert.assertNotNull(educationInfo);
    }

    @Test(expected = customUncheckedException.class)
    public void handleFailTest(){

        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");
        Mockito.doThrow(customUncheckedException.class).when(educationInfoServiceObj).fetchRequiredData(inputMasterObj);
        List<EducationInfo> educationInfo=educationInfohandlerObj.handle(inputMasterObj);

    }
}
