package com.personal.informationendpoint.handler;

import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;
import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import com.personal.informationendpoint.pojo.response.PersonalInfo;
import com.personal.informationendpoint.service.educationInfoService;
import com.personal.informationendpoint.service.experienceInfoService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class experienceInfohandlerTest {


    @InjectMocks
    private ExperienceInfohandler experienceInfohandlerObj;

    @Mock
    private experienceInfoService experienceInfoServiceObj;

    List<ExperienceInfo> experienceInfoObj;


    @Before
    public void init()
    {
        MockitoAnnotations.initMocks(this);

        ExperienceInfo experienceInfo=new ExperienceInfo();
        experienceInfo.setTechStack("");
        experienceInfo.setTeam("Home Delivery");
        experienceInfo.setStartDate(new Date());
        experienceInfo.setRoles("");
        experienceInfo.setResponsibilities("");
        experienceInfo.setProject("");
        experienceInfo.setPersonID(1);
        experienceInfo.setEndDate(new Date());
        experienceInfo.setContractingCompany("");
        experienceInfo.setClientCompany("");

        experienceInfoObj= new ArrayList<>();
        experienceInfoObj.add(experienceInfo);
    }

    @Test
    public void handlePassTest(){

        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");

        Mockito.when(experienceInfoServiceObj.fetchRequiredData(inputMasterObj)).thenReturn(experienceInfoObj);
        List<ExperienceInfo>  experienceInfoObj=experienceInfohandlerObj.handle(inputMasterObj);
        Assert.assertNotNull(experienceInfoObj);
    }

    @Test(expected = customUncheckedException.class)
    public void handleFailTest(){

        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");
        Mockito.doThrow(customUncheckedException.class).when(experienceInfoServiceObj).fetchRequiredData(inputMasterObj);
        List<ExperienceInfo>  experienceInfoObj=experienceInfohandlerObj.handle(inputMasterObj);

    }
}
