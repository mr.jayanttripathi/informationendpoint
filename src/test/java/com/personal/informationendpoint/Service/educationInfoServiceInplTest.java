package com.personal.informationendpoint.Service;

import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.dao.impl.educationInfoDaoImpl;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;
import com.personal.informationendpoint.service.impl.educationInfoServiceInpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class educationInfoServiceInplTest {

    @Mock
    private educationInfoDaoImpl personalInfoDaoImplObj;

    @InjectMocks
    private educationInfoServiceInpl educationInfoServiceInplObj;


    List<EducationInfo> educationInfoObj;

    @Before
    public void init()
    {
        MockitoAnnotations.initMocks(this);

        EducationInfo educationInfo=new EducationInfo();
        educationInfo.setResult("");
        educationInfo.setPersonID(1);
        educationInfo.setInstitution("College");
        educationInfo.setGraduationDate("");
        educationInfo.setDegree("");

        educationInfoObj=new ArrayList<>();
        educationInfoObj.add(educationInfo);

    }

    @Test
    public void fetchRequiredDataPassTest()
    {
        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");

        Mockito.when(personalInfoDaoImplObj.fetchRequestedData(inputMasterObj)).thenReturn(educationInfoObj);

        List<EducationInfo> educationInfo=educationInfoServiceInplObj.fetchRequiredData(inputMasterObj);
        Assert.assertNotNull(educationInfo);
    }

    @Test(expected = customUncheckedException.class)
    public void fetchRequiredDataFailTest()
    {
        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");
        
        Mockito.doThrow(customUncheckedException.class).when(personalInfoDaoImplObj).fetchRequestedData(inputMasterObj);
        List<EducationInfo> educationInfo=educationInfoServiceInplObj.fetchRequiredData(inputMasterObj);

    }


}
