package com.personal.informationendpoint.Service;

import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.dao.impl.educationInfoDaoImpl;
import com.personal.informationendpoint.dao.impl.experienceInfoDaoImpl;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;
import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import com.personal.informationendpoint.service.impl.educationInfoServiceInpl;
import com.personal.informationendpoint.service.impl.experienceInfoServiceInpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class experienceInfoServiceInplTest {


    @Mock
    private experienceInfoDaoImpl experienceInfoDaoObj;

    @InjectMocks
    private experienceInfoServiceInpl experienceInfoServiceInplObj;


    List<ExperienceInfo> experienceInfoObj;

    @Before
    public void init()
    {
        MockitoAnnotations.initMocks(this);

        ExperienceInfo experienceInfo=new ExperienceInfo();
        experienceInfo.setTechStack("");
        experienceInfo.setTeam("Home Delivery");
        experienceInfo.setStartDate(new Date());
        experienceInfo.setRoles("");
        experienceInfo.setResponsibilities("");
        experienceInfo.setProject("");
        experienceInfo.setPersonID(1);
        experienceInfo.setEndDate(new Date());
        experienceInfo.setContractingCompany("");
        experienceInfo.setClientCompany("");

        experienceInfoObj= new ArrayList<>();
        experienceInfoObj.add(experienceInfo);
    }

    @Test
    public void fetchRequiredDataPassTest()
    {
        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");

        Mockito.when(experienceInfoDaoObj.fetchRequestedData(inputMasterObj)).thenReturn(experienceInfoObj);

        List<ExperienceInfo> experienceInfo=experienceInfoServiceInplObj.fetchRequiredData(inputMasterObj);
        Assert.assertNotNull(experienceInfo);
    }

    @Test(expected = customUncheckedException.class)
    public void fetchRequiredDataFailTest()
    {
        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");

        Mockito.doThrow(customUncheckedException.class).when(experienceInfoDaoObj).fetchRequestedData(inputMasterObj);
        List<ExperienceInfo> experienceInfos=experienceInfoServiceInplObj.fetchRequiredData(inputMasterObj);

    }

}
