package com.personal.informationendpoint.Service;

import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.dao.impl.experienceInfoDaoImpl;
import com.personal.informationendpoint.dao.impl.personalInfoDaoImpl;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import com.personal.informationendpoint.pojo.response.PersonalInfo;
import com.personal.informationendpoint.service.impl.experienceInfoServiceInpl;
import com.personal.informationendpoint.service.impl.personalInfoServiceInpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class personalInfoServiceInplTest {


    @Mock
    private personalInfoDaoImpl personalInfoDaoObj;

    @InjectMocks
    private personalInfoServiceInpl personalInfoServiceInplObj;


    PersonalInfo personalInfoObj;

    @Before
    public void init()
    {
        MockitoAnnotations.initMocks(this);
        personalInfoObj=new PersonalInfo();
        personalInfoObj.setFirstName("Jayant");
        personalInfoObj.setCity("Phoenix");
        personalInfoObj.setCountry("United States");
        personalInfoObj.setDateofBirth(new Date());
        personalInfoObj.setEmailAddress("Mr.JayantTripathi@Gmail.com");
        personalInfoObj.setLastName("Tripathi");
        personalInfoObj.setMiddleName("");
        personalInfoObj.setPhoneNUmber("6825600224");
        personalInfoObj.setState("");
        personalInfoObj.setStreet("43rd");
        personalInfoObj.setSuite("215");
    }

    @Test
    public void fetchRequiredDataPassTest()
    {
        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");

        Mockito.when(personalInfoDaoObj.fetchRequestedData(inputMasterObj)).thenReturn(personalInfoObj);

        PersonalInfo personalInfo=personalInfoServiceInplObj.fetchRequiredData(inputMasterObj);
        Assert.assertNotNull(personalInfo);
    }

    @Test(expected = customUncheckedException.class)
    public void fetchRequiredDataFailTest()
    {
        InputMaster inputMasterObj=new InputMaster();
        inputMasterObj.setPersonID("1");

        Mockito.doThrow(customUncheckedException.class).when(personalInfoDaoObj).fetchRequestedData(inputMasterObj);
        PersonalInfo personalInfo=personalInfoServiceInplObj.fetchRequiredData(inputMasterObj);

    }
}
