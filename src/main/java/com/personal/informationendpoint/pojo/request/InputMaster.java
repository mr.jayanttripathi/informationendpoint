package com.personal.informationendpoint.pojo.request;

public class InputMaster {

    private String personID;


    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }
}
