package com.personal.informationendpoint.controller;

import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.handler.educationInfohandler;
import com.personal.informationendpoint.handler.ExperienceInfohandler;
import com.personal.informationendpoint.handler.persoanlInfohandler;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;
import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import com.personal.informationendpoint.pojo.response.PersonalInfo;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class InformationController {


  @Autowired
  persoanlInfohandler persoanlInfohandlerObj;

  @Autowired
  educationInfohandler educationInfohandlerObj;

  @Autowired
  ExperienceInfohandler experienceInfohandlerObj;

  Logger logger = LoggerFactory.getLogger(PersonalInfo.class);

  @ApiOperation(value = "Personal Information LookUp Service")
  @PostMapping(path = "/PersonalInfo")
  public @ResponseBody PersonalInfo getPersonalInfo(@RequestBody InputMaster inputMaster) {


    logger.info("Personal Information Controller is being executed");
    PersonalInfo personalInfo;

    try {

      personalInfo=persoanlInfohandlerObj.handle(inputMaster);

    } catch (customUncheckedException runtimeException) {
      logger.error("Error Message : " + runtimeException.getMessage());
      logger.error("Error Cause : " + runtimeException.getCause());
      throw new customUncheckedException(
          "This unchecked exception is thrown in the PersonalInfo Controller.\n "

              + runtimeException.getMessage(),
          runtimeException);
    }
    return personalInfo;
  }


  @ApiOperation(value = "Education Information LookUp Service")
  @PostMapping(path = "/EducationInfo")
  public @ResponseBody  List<EducationInfo> getEducationInfo(@RequestBody InputMaster inputMaster) {


    logger.info("Education Information Controller is being executed");
    List<EducationInfo>  educationInfo;

    try {

      educationInfo=educationInfohandlerObj.handle(inputMaster);

    } catch (customUncheckedException runtimeException) {
      logger.error("Error Message : " + runtimeException.getMessage());
      logger.error("Error Cause : " + runtimeException.getCause());
      throw new customUncheckedException(
              "This unchecked exception is thrown in the EducationInfo Controller.\n "

                      + runtimeException.getMessage(),
              runtimeException);
    }
    return educationInfo;
  }



  @ApiOperation(value = "Experience Information LookUp Service")
  @PostMapping(path = "/ExperienceInfo")
  public @ResponseBody  List<ExperienceInfo> getExperienceInfo(@RequestBody InputMaster inputMaster) {


    logger.info("Personal Information Controller is being executed");
    List<ExperienceInfo> experienceInfo;

    try {

      experienceInfo=experienceInfohandlerObj.handle(inputMaster);

    } catch (customUncheckedException runtimeException) {
      logger.error("Error Message : " + runtimeException.getMessage());
      logger.error("Error Cause : " + runtimeException.getCause());
      throw new customUncheckedException(
              "This unchecked exception is thrown in the ExperienceInfo Controller.\n "

                      + runtimeException.getMessage(),
              runtimeException);
    }
    return experienceInfo;
  }

}
