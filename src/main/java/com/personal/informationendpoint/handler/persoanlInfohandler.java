package com.personal.informationendpoint.handler;


import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import com.personal.informationendpoint.pojo.response.PersonalInfo;
import com.personal.informationendpoint.service.experienceInfoService;
import com.personal.informationendpoint.service.personalInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class persoanlInfohandler {

    Logger logger = LoggerFactory.getLogger(ExperienceInfohandler.class);

    @Autowired
    personalInfoService personalInfoServiceObj;

    public PersonalInfo handle(InputMaster inputMaster)
    {
        logger.info("Experience Information Handler is being executed");

        PersonalInfo personalInfo= null;
        try
        {
        personalInfo = personalInfoServiceObj.fetchRequiredData(inputMaster);
        }
        catch(customUncheckedException ex)        {
            logger.error(ex.getMessage());
            throw new customUncheckedException(
                    "This unchecked exception is thrown in the PersonalInfo Controller.\n "+ ex.getMessage(),ex);
        }

        return personalInfo;
    }


}
