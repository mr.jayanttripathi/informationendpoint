package com.personal.informationendpoint.handler;


import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;
import com.personal.informationendpoint.pojo.response.PersonalInfo;
import com.personal.informationendpoint.service.educationInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class educationInfohandler {

    Logger logger = LoggerFactory.getLogger(educationInfohandler.class);

    @Autowired
    educationInfoService educationInfoServiceObj;

    public List<EducationInfo> handle(InputMaster inputMaster)
    {
        logger.info("Education Information Handler is being executed");

        List<EducationInfo> educationInfo=null;
        try
        {
            educationInfo =educationInfoServiceObj.fetchRequiredData(inputMaster);
        }
        catch(customUncheckedException ex){
            logger.error(ex.getMessage());
            throw new customUncheckedException(
                    "This unchecked exception is thrown in the PersonalInfo Controller.\n "+ ex.getMessage(),ex);
        }

        return educationInfo;
    }

}
