package com.personal.informationendpoint.dao.mapper;

import com.personal.informationendpoint.pojo.response.PersonalInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class personalInfoMapper implements RowMapper<PersonalInfo> {


    @Override
    public PersonalInfo mapRow(ResultSet resultSet, int i) throws SQLException {

        PersonalInfo  personalInfo=new PersonalInfo();
        personalInfo.setFirstName(resultSet.getString("FirstName"));
        personalInfo.setMiddleName(resultSet.getString("MiddleName"));
        personalInfo.setLastName(resultSet.getString("LastName"));
        personalInfo.setSuite(resultSet.getString("Suite"));
        personalInfo.setStreet(resultSet.getString("Street"));
        personalInfo.setCity(resultSet.getString("City"));
        personalInfo.setState(resultSet.getString("State"));
        personalInfo.setCountry(resultSet.getString("Country"));
        personalInfo.setPhoneNUmber(resultSet.getString("PhoneNUmber"));
        personalInfo.setDateofBirth(resultSet.getDate("DateofBirth"));
        personalInfo.setEmailAddress(resultSet.getString("EmailAddress"));



        return personalInfo;
    }


}
