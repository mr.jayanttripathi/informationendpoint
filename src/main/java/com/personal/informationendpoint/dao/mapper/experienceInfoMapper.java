package com.personal.informationendpoint.dao.mapper;

import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class experienceInfoMapper implements RowMapper<ExperienceInfo> {



    @Override
    public ExperienceInfo mapRow(ResultSet resultSet, int i) throws SQLException {

        ExperienceInfo  experienceInfo=new ExperienceInfo();
        experienceInfo.setClientCompany(resultSet.getString("ClientCompany"));
        experienceInfo.setContractingCompany(resultSet.getString("ContractingCompany"));
        experienceInfo.setEndDate(resultSet.getDate("EndDate"));
        experienceInfo.setPersonID(resultSet.getInt("PersonID"));
        experienceInfo.setProject(resultSet.getString("Project"));
        experienceInfo.setResponsibilities(resultSet.getString("Responsibilities"));
        experienceInfo.setRoles(resultSet.getString("Roles"));
        experienceInfo.setStartDate(resultSet.getDate("StartDate"));
        experienceInfo.setTeam(resultSet.getString("Team"));
        experienceInfo.setTechStack(resultSet.getString("TechStack"));

        return experienceInfo;
    }
}
