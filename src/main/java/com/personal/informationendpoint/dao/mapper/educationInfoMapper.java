package com.personal.informationendpoint.dao.mapper;

import com.personal.informationendpoint.pojo.response.EducationInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class educationInfoMapper implements RowMapper<EducationInfo> {

    @Override
    public EducationInfo mapRow(ResultSet resultSet, int i) throws SQLException {

        EducationInfo  educationInfo=new EducationInfo();
        educationInfo.setDegree(resultSet.getString("Degree"));
        educationInfo.setGraduationDate(resultSet.getString("GraduationDate"));
        educationInfo.setInstitution(resultSet.getString("Institution"));
        educationInfo.setPersonID(resultSet.getInt("PersonID"));
        educationInfo.setResult(resultSet.getString("Result"));

        return educationInfo;
    }
}
