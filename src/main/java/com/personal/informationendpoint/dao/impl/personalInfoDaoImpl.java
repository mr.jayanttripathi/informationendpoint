package com.personal.informationendpoint.dao.impl;


import com.personal.informationendpoint.dao.mapper.personalInfoMapper;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.PersonalInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.apache.commons.io.IOUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Repository
public class personalInfoDaoImpl {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplateObj;

    @Value("/DatabaseQueries/personalInfoSql.sql")
    private String query;

    @PostConstruct
    public void init() throws IOException {
        query= IOUtils.toString(this.getClass().getResourceAsStream(query));
    }

    public PersonalInfo fetchRequestedData(InputMaster inputMaster)
    {
        Map<String, Integer> parameterSource= new HashMap<>();
        parameterSource.put("personalID",1);
        PersonalInfo personalInfo = namedParameterJdbcTemplateObj.queryForObject(query,parameterSource,new personalInfoMapper());
        return personalInfo;
    }

}
