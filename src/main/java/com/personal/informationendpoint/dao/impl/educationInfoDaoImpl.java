package com.personal.informationendpoint.dao.impl;


import com.personal.informationendpoint.dao.mapper.educationInfoMapper;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;
import com.personal.informationendpoint.pojo.response.ExperienceInfo;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class educationInfoDaoImpl {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplateObj;

    @Value("/DatabaseQueries/educationInfoSql.sql")
    private String query;

    @PostConstruct
    public void init() throws IOException {
        query= IOUtils.toString(this.getClass().getResourceAsStream(query));
    }

    public List<EducationInfo> fetchRequestedData(InputMaster inputMaster)
    {
        Map<String, Integer> parameterSource= new HashMap<>();
        parameterSource.put("personalID",1);
        EducationInfo educationInfo=new EducationInfo();
        List<EducationInfo> data=new ArrayList<>();
        data = namedParameterJdbcTemplateObj.query(query,parameterSource,new educationInfoMapper());
        return data;
    }
}
