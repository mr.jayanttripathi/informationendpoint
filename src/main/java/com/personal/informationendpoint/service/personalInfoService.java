package com.personal.informationendpoint.service;

import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.PersonalInfo;

public interface personalInfoService {


    PersonalInfo fetchRequiredData(InputMaster inputMaster);
}
