package com.personal.informationendpoint.service;

import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.ExperienceInfo;

import java.util.List;

public interface experienceInfoService {

    List<ExperienceInfo> fetchRequiredData(InputMaster inputMaster);
}
