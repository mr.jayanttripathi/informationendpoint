package com.personal.informationendpoint.service;

import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;

import java.util.List;

public interface educationInfoService {

    List<EducationInfo> fetchRequiredData(InputMaster inputMaster);
}
