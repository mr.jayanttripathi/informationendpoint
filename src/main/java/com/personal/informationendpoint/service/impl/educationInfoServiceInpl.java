package com.personal.informationendpoint.service.impl;

import com.personal.informationendpoint.common.exceptions.customUncheckedException;
import com.personal.informationendpoint.dao.impl.educationInfoDaoImpl;
import com.personal.informationendpoint.handler.educationInfohandler;
import com.personal.informationendpoint.pojo.request.InputMaster;
import com.personal.informationendpoint.pojo.response.EducationInfo;
import com.personal.informationendpoint.service.educationInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class educationInfoServiceInpl implements educationInfoService {

    Logger logger = LoggerFactory.getLogger(educationInfohandler.class);

    @Autowired
    educationInfoDaoImpl personalInfoDaoImplObj;

    @Override
    public List<EducationInfo>  fetchRequiredData(InputMaster inputMaster) {
        logger.info("Education Information Handler is being executed");

        List<EducationInfo> educationInfo=null;

        try {
            educationInfo = personalInfoDaoImplObj.fetchRequestedData(inputMaster);
       }
         catch(customUncheckedException ex)        {
            logger.error(ex.getMessage());
            throw new customUncheckedException(
                    "This unchecked exception is thrown in the PersonalInfo Controller.\n "+ ex.getMessage(),ex);
        }
        return educationInfo;

    }
}
